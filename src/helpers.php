<?php

if(!function_exists('hasPackage')) {
    /**
     * check if the package exist or not
     *
     * @param string $packageName
     * @return boolean
     */
    function hasPackage(string $packageName)
    {
        return app('composerHasable')->has($packageName);
    }
}
<?php

namespace KyoConseil\ComposerHasable;

use Illuminate\Support\Facades\File;

/**
 * composer check for required packages
 * 
 * @method boolean has(string $packageName)
 */
class ComposerHasable {

    /**
     * all the composer required packaes
     *
     * @var array
     */
    private $requiredPackages = [];

    /**
     * init the required packages
     */
    public function __construct()
    {
        $this->requiredPackages = $this->getRequiredPackages();
    }

    /**
     * get all the required packages from composer file
     *
     * @return array
     */
    private function getRequiredPackages()
    {
        $composerJsonPath = base_path('composer.json');
        $composerJsonContents = File::get($composerJsonPath);
        $composerData = json_decode($composerJsonContents, true);
        return $composerData['require'] ?? [];
    }

    /**
     * check if package is required in composer or no
     *
     * @param string $packageName
     * @return boolean
     */
    public function has(string $packageName)
    {
        return (bool) ($this->requiredPackages[$packageName] ?? false);
    }
}
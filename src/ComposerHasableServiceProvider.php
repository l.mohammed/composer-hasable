<?php

namespace KyoConseil\ComposerHasable;

use Illuminate\Support\ServiceProvider;
use KyoConseil\ComposerHasable\ComposerHasable;
use Illuminate\Support\Facades\Blade;

class ComposerHasableServiceProvider extends ServiceProvider
{
    /**
     * boot the composer hasable service into laravel
     *
     * @return void
     */
    public function boot()
    {
        $this->app->singleton('composerHasable', function() {
            return new ComposerHasable();
        });

        Blade::if('hasPackage', function ($package) {
            $composerHasable = app('composerHasable');

            return $composerHasable->has($package);
        });
    }
}
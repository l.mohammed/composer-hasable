<h1 align="center">Laravel-Composer-Hasable</h1>

**Laravel-Composer-Hasable** is a Laravel package that provides an easy way to check if package is required or not.

## Features
- check if package is required.
- use custom directive to check if package is required.

## Installation
### Step 1 - Require the package
    composer require kyoconseil/composer-hasable

### Step 2: Require Service Provider
in the `app.php` config file add
```php
'providers' => [
    ...
    KyoConseil\ComposerHasable\ComposerHasableServiceProvider::class,
];
```

## Uses
### Use `hasPackage()` for check
you can use hasPackage('acme/package')  for check if the package is required or not
example:

```php
$canUseLaravelTinker = hasPackage('laravel/tinker');
dump($canUseLaravelTinker); //true

$canUsePotato = hasPackage('potato');
dump($canUsePotato) //false
```
### Use the custom directive `@hasPackage() ... @endhasPackage` for check
you can use `@hasPackage('acme/package') <!-- HTML code here --> @endhasPackage` for check and render the html code if the package is required
example:

```html
<p>show the text if tinker laravel is required</p>

@hasPackage('laravel/tinker')
<p>yes you can use laravel tinker</p>
@endhasPackage

<p>show the text if potato package is required</p>

@hasPackage('potato')
<p>you can use potato package</p>
@endhasPackage
```